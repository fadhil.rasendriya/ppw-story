from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from .models import MataKuliah

# Create your views here.
from matakuliah.forms import MataKuliahForm


def create_matakuliah_view(request):
    context = {}
    if request.method == 'POST':
        form = MataKuliahForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/matakuliah/')
    else:
        form = MataKuliahForm()
    context['form'] = form
    return render(request, 'matakuliah/create_matakuliah_view.html', context)


def matakuliah(request):
    context = {'list_matkul': MataKuliah.objects.all()}
    total_sks = 0
    for matkul in context['list_matkul']:
        total_sks += matkul.sks
    context['total_sks'] = total_sks
    return render(request, 'matakuliah/matakuliah.html', context)


def delete_matkul(request):
    if request.method == 'POST':
        matkul_id = request.POST['matkul_id']
        obj = get_object_or_404(MataKuliah, id=matkul_id)
        obj.delete()
    return HttpResponseRedirect('/matakuliah')


def detail_matkul_view(request, id):
    matkul = get_object_or_404(MataKuliah, id=id)
    context = {'matkul': matkul}
    return render(request, 'matakuliah/detail_matkul.html', context)
