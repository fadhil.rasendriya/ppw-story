from django.db import models


# Create your models here.
class MataKuliah(models.Model):

    TAHUN = tuple([(f"{x}/{x+1}", f"{x}/{x+1}") for x in range(2019, 2025, 1)])
    SEMESTER = (
        ('Ganjil', 'Ganjil'),
        ('Genap', 'Genap'),
        )

    # Fields
    SKS = tuple([(x, x) for x in range(1, 7, 1)])
    nama_matakuliah = models.CharField(max_length=255)
    nama_dosen = models.CharField(max_length=255)
    sks = models.IntegerField(choices=SKS)
    deskripsi = models.TextField()
    semester = models.CharField(max_length=255, choices=SEMESTER)
    tahun = models.CharField(max_length=255, choices=TAHUN)
    ruang_kelas = models.CharField(max_length=255)

    def __str__(self):
        return self.nama_matakuliah
