from django.test import TestCase, Client
from django.urls.base import resolve
from . import views
from .models import Todo, Person
from .forms import TodoForm, PersonForm
class KegiatanTestCase(TestCase):

    def test_kegiatan_url_exists(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_using_index_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, views.index)

    def test_model_can_create_new_todo(self):
        Todo.objects.create(title='mengerjakan ppw', description='mengerjakan story 5 ppw')
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = TodoForm(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_kegiatan_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/kegiatan/add-todo/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_kegiatan_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/kegiatan/add-todo/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_model_person_can_create_new(self):
        todo = Todo.objects.create(title='Test', description='Test')
        Person.objects.create(name='Anonymous', todo=todo)
        counting_all_available_person = Person.objects.all().count()
        self.assertEqual(counting_all_available_person, 1)

    def test_validation_for_person_form_empty(self):
        form = PersonForm(data={'name': '', 'todo': None})
        self.assertFalse(form.is_valid())

    def test_person_post_success_and_render_result(self):
        test = 'Anonymous'
        a = Todo.objects.create(title=test, description=test)
        response_post = Client().post('/kegiatan/add-person/', {'name': test, 'todo': a})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_person_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/kegiatan/add-person/', {'name': '', 'todo': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
