# Generated by Django 3.1.1 on 2020-10-24 03:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0003_auto_20201023_1858'),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('todo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kegiatan.todo')),
            ],
        ),
    ]
