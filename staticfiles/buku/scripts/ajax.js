$('#searchButton').click(function () {
    let bookTitle = $('#searchBook').val();
    $.ajax({
        method: 'GET',
        data: {
            query: bookTitle,
        },
        url: "/buku/getBook/",
        success: function (result) {
            let bookContainer = $('#hasilCari');
            bookContainer.empty();
            const bookResults = result.items;
            for (let i = 0; i < bookResults.length; i++) {
                let volumeInfo = bookResults[i].volumeInfo;
                let title = volumeInfo.title;
                let publisher = volumeInfo.publisher;
                let publishedDate = volumeInfo.publishedDate;
                let thumbnail;
                try{
                    thumbnail = volumeInfo.imageLinks.thumbnail;
                } catch (e) {
                    thumbnail = "None";
                }

                bookContainer.append(
                    "<div class=\"row text-light bg-palette-3-dark m-4\">\n" +
                    "  <img class='m-5'  src='" + thumbnail + "' alt=\"img\">\n" +
                    "    <table class='m-5 p-5'>\n" +
                    "      <tr>\n" +
                    "        <td>Title: </td>\n" +
                    "        <td>" + title + "</td>\n" +
                    "      </tr>\n" +
                    "      <tr>\n" +
                    "         <td>Publisher: </td>\n" +
                    "         <td>" + publisher + "</td>\n" +
                    "      </tr>\n" +
                    "      <tr>\n" +
                    "       <td>Published Date: </td>\n" +
                    "       <td>" + publishedDate + "</td>\n" +
                    "      </tr>\n" +
                    "    </table>\n" +
                    "</div>")
            }

        }
    });


})

