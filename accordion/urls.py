from . import views
from django.urls import path

app_name = 'accordion'

urlpatterns = [
    path('', views.index, name='index'),
]