from django.test import TestCase, Client
from . import views
from django.urls.base import resolve


# Create your tests here.
class BukuTestCase(TestCase):

    def test_buku_url_exists(self):

        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, views.index)

    def test_ajax_redirects_json(self):
        response = Client().get('/buku/getBook/', data={'query': 'calculus'})
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 302)



