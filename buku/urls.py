from . import views
from django.urls import path

app_name = 'buku'

urlpatterns = [
    path('', views.index, name='index'),
    path('getBook/', views.get_book_response, name='get_book'),
]