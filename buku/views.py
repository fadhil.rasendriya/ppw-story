
from django.shortcuts import render, redirect
from django.http import JsonResponse

# Create your views here.
def index(request):
    return render(request, 'buku/index.html')

def get_book_response(request):
    search_query = request.GET.get('query')
    # Ganti API Key dengan key pribadi
    url = f"https://www.googleapis.com/books/v1/volumes?q={search_query}"
    return redirect(url)


