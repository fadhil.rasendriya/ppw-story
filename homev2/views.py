from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, 'homev2/index.html')


def gallery(request):
    return render(request, 'homev2/gallery.html')
